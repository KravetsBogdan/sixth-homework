//1) Екранування нам потрібно для написання певних символів на сайті,які зарезервовані в Js. Я використав екранування на 6 строкі.
// В даному випадку цей знак ( ' ) зарезервований в js і закриває рядок в непотрібному мені місці. Екранування ж передає цей знак 
// як звичайний рядковий знак
//2) Мені відомо 3 способи оголошення функції: 1)Звичайне оголошення функції,2)функціональний вираз,3)стрілочна функція
//  1) function name (arg){  //звичайне оголошення функції
//  тіло функції
// };
// name();
//  2) let name = function(){    // функціональний вираз
//    
// };
// name();
//  3) let name = () => {};  // стрілочна функція   
//3)Hoisting, тобто спливання, підняття, це механізм, при якому змінні та оголошення функції піднімаються вгору по своїй області
//видимості перед виконанням коду. Однією з переваг підйому є те, що він дозволяє нам використовувати функції перед їх 
// оголошенням у коді.

let newUser = {};
function createNewUser(firstName, lastName, date) {
    while (!firstName || !isNaN(firstName)) {
        firstName = prompt('Введіть ваше ім\'я')?.trim();
    }
    while (!lastName || !isNaN(lastName)) {
        lastName = prompt('Введіть вашу фамілію')?.trim();
    }
    date = prompt("Enter your date of birth as dd/mm/yyyy");

    let birthDate = date.split("/")[0];
    let birthMonth = date.split("/")[1];
    let birthYear = date.split("/")[2];
    let userDateOfBirth = new Date(birthYear, birthMonth, birthDate);
    newUser = {
        firstName,
        lastName,
        birthday: userDateOfBirth,
        getLogin() {
            let login = this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
            return login;
        },
        getAge() {
            let currentDate = new Date();
            let userAge = Math.floor(
                (currentDate - this.birthday) / (1000 * 60 * 60 * 24 * 365)
            );
            return `Тобі ${userAge} років`;
        },
        getPassword() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear();

        },
    };
    return newUser;
};
createNewUser();
console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());

